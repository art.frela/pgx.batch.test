[![pipeline status](https://gitlab.com/art.frela/pgx.batch.test/badges/master/pipeline.svg)](https://gitlab.com/art.frela/pgx.batch.test/-/commits/master)

# pgx.batch.test

Go test cases for github.com/jackc/pgx/v4 batch queries

## Main thought

If one or more queries contains error, then test freeze happen.

Example:
```bash
go test -v                     
=== RUN   TestMigrationUP
=== RUN   TestMigrationUP/postgres9
    TestMigrationUP/postgres9: pgx_test.go:48: inserted 5 rows
=== RUN   TestMigrationUP/postgres10
    TestMigrationUP/postgres10: pgx_test.go:48: inserted 5 rows
=== RUN   TestMigrationUP/postgres11
    TestMigrationUP/postgres11: pgx_test.go:48: inserted 5 rows
=== RUN   TestMigrationUP/postgres12
    TestMigrationUP/postgres12: pgx_test.go:48: inserted 5 rows
--- PASS: TestMigrationUP (0.10s)
    --- PASS: TestMigrationUP/postgres9 (0.02s)
    --- PASS: TestMigrationUP/postgres10 (0.03s)
    --- PASS: TestMigrationUP/postgres11 (0.03s)
    --- PASS: TestMigrationUP/postgres12 (0.02s)
=== RUN   TestGetSamples
=== RUN   TestGetSamples/postgres9
    TestGetSamples/postgres9: pgx_test.go:58: start test case - postgres9
    TestGetSamples/postgres9: pgx_test.go:73: inserted 5 rows
    TestGetSamples/postgres9: pgx_test.go:76: expected success GetSamples but error, batch query error ERROR: syntax error at end of input (SQLSTATE 42601)
    TestGetSamples/postgres9: pgx_test.go:79: expected count >0, but got 0
    TestGetSamples/postgres9: pgx_test.go:81: samples: []
# FREEZE..... while Ctrl+C
^Csignal: interrupt
FAIL    gitlab.com/art.frela/pgxtest    99.704s
```

In simple example all ok, main func ends without freeze

## Test environment

- OS: macOS Catalina 10.15.5 
- docker desktop 2.3.0.3, engine 19.03.8
- docker-compose 1.25.5
- go version go1.14.2 darwin/amd64
- github.com/jackc/pgconn v1.6.0
- github.com/jackc/pgx/v4 v4.6.0
- docker-compose.yml in the repo's root
