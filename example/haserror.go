package main

import (
	"log"
	"time"

	"gitlab.com/art.frela/pgxtest"
)

const (
	pg12    string        = "postgres://sa:qwerty@localhost:45432/sa?sslmode=disable&pool_max_conns=2"
	timeout time.Duration = 10 * time.Second
)

func main() {
	pg, err := pgxtest.NewPGRepo(pg12, timeout)
	if err != nil {
		log.Fatalf("pgrepo connection error, %v", err)
	}
	defer pg.Close()
	defer pg.MigrationDOWN()
	err = pg.MigrationUP()
	if err != nil {
		log.Fatalf("MigrationUP error, %v", err)
	}
	c, err := pg.InsertSampleData()
	if err != nil {
		log.Fatalf("expected success InsertSampleData but error, %v\n", err)
	}
	log.Printf("inserted %d rows\n", c)
	samples, count, err := pg.GetSamples()
	if err != nil {
		log.Fatalf("expected success GetSamples but error, %v", err)
	}
	log.Printf("samples[%d]: %+v\n", count, samples)
}
