module gitlab.com/art.frela/pgxtest

go 1.14

require (
	github.com/jackc/pgconn v1.6.0 // indirect
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jackc/puddle v1.1.1 // indirect
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
