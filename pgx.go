package pgxtest

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Sample struct {
	ID        int32
	Data      string
	CreatedAt time.Time
}

type Samples []Sample

func (pgr *PGRepo) GetSamples() (Samples, int64, error) {
	query1 := `select id, data, created_at
			from public.sample
			where ` // specify error for
	query2 := `select count(*) from public.sample`

	batch := &pgx.Batch{}
	batch.Queue(query1)
	batch.Queue(query2)

	ctx, cancel := context.WithTimeout(context.Background(), pgr.timeout)
	defer cancel()

	batchResults := pgr.db.SendBatch(ctx, batch)
	rows, err := batchResults.Query()
	if err != nil {
		return nil, 0, fmt.Errorf("batch query error %v", err)
	}
	defer batchResults.Close()
	defer rows.Close()

	result := make(Samples, 0)
	for rows.Next() {
		item := Sample{}
		err = rows.Scan(&item.ID, &item.Data, &item.CreatedAt)
		if err != nil {
			if err == pgx.ErrNoRows {
				continue
			}
			return nil, 0, fmt.Errorf("query [%s], error %v", query1, err)
		}
		result = append(result, item)
	}
	var count int64
	err = batchResults.QueryRow().Scan(&count)
	if err != nil {
		return nil, count, fmt.Errorf("query [%s] error, %v", query2, err)
	}
	return result, count, nil
}

// helpers:

type PGRepo struct {
	connString string
	timeout    time.Duration
	db         *pgxpool.Pool
}

func NewPGRepo(connString string, timeout time.Duration) (*PGRepo, error) {
	pgr := &PGRepo{
		connString: connString,
		timeout:    timeout,
	}
	err := pgr.connDB()
	if err != nil {
		return nil, err
	}
	return pgr, nil
}

// connDB - make connection to database
func (pgr *PGRepo) connDB() error {
	config, err := pgxpool.ParseConfig(pgr.connString)
	if err != nil {
		return err
	}
	pool, err := pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		return err
	}
	pgr.db = pool
	return nil
}

// Close - close connection to database
func (pgr *PGRepo) Close() {
	if pgr != nil && pgr.db != nil {
		pgr.db.Close()
	}
}

func (pgr *PGRepo) MigrationUP() error {
	ctx, cancel := context.WithTimeout(context.Background(), pgr.timeout)
	defer cancel()

	queryTBL := `create table if not exists sample (
			id SMALLSERIAL PRIMARY KEY,
		   data VARCHAR NOT NULL,
		   created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP);`
	_, err := pgr.db.Exec(ctx, queryTBL)
	if err != nil {
		return fmt.Errorf("create sample table error, %v", err)
	}
	return nil
}
func (pgr *PGRepo) MigrationDOWN() error {
	ctx, cancel := context.WithTimeout(context.Background(), pgr.timeout)
	defer cancel()

	queryTBL := `drop table sample;`
	_, err := pgr.db.Exec(ctx, queryTBL)
	if err != nil {
		return fmt.Errorf("drop sample table error, %v", err)
	}
	return nil
}

func (pgr *PGRepo) InsertSampleData() (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), pgr.timeout)
	defer cancel()

	queryTBL := `insert into sample(data) values
	('sample1'),('sample2'),('sample3'),('sample4'),('sample5');`
	res, err := pgr.db.Exec(ctx, queryTBL)
	if err != nil {
		return 0, fmt.Errorf("create sample table error, %v", err)
	}
	return res.RowsAffected(), nil
}
