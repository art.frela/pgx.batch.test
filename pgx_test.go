package pgxtest

import (
	"testing"
	"time"
)

const (
	pg9     string        = "postgres://sa:qwerty@localhost:15432/sa?sslmode=disable&pool_max_conns=2"
	pg10    string        = "postgres://sa:qwerty@localhost:25432/sa?sslmode=disable&pool_max_conns=2"
	pg11    string        = "postgres://sa:qwerty@localhost:35432/sa?sslmode=disable&pool_max_conns=2"
	pg12    string        = "postgres://sa:qwerty@localhost:45432/sa?sslmode=disable&pool_max_conns=2"
	timeout time.Duration = 10 * time.Second
)

type testcase struct {
	title string
	cs    string
}

var (
	cases []testcase = []testcase{
		testcase{"postgres9", pg9},
		testcase{"postgres10", pg10},
		testcase{"postgres11", pg11},
		testcase{"postgres12", pg12},
	}
)

func TestMigrationUP(t *testing.T) {

	for _, tcase := range cases {
		t.Run(tcase.title, func(t *testing.T) {
			pg, err := NewPGRepo(tcase.cs, timeout)
			if err != nil {
				t.Errorf("expected success pgrepo connection, but error, %v", err)
			}
			defer pg.Close()
			defer pg.MigrationDOWN()
			err = pg.MigrationUP()
			if err != nil {
				t.Errorf("expected success MigrationUP but error, %v", err)
			}
			c, err := pg.InsertSampleData()
			if err != nil {
				t.Errorf("expected success InsertSampleData but error, %v", err)
			}
			t.Logf("inserted %d rows", c)
		})
	}

}

func TestGetSamples(t *testing.T) {

	for _, tcase := range cases {
		t.Run(tcase.title, func(t *testing.T) {
			t.Logf("start test case - %s\n", tcase.title)
			pg, err := NewPGRepo(tcase.cs, timeout)
			if err != nil {
				t.Errorf("expected success pgrepo connection, but error, %v", err)
			}
			defer pg.Close()
			defer pg.MigrationDOWN()
			err = pg.MigrationUP()
			if err != nil {
				t.Errorf("expected success MigrationUP but error, %v", err)
			}
			c, err := pg.InsertSampleData()
			if err != nil {
				t.Errorf("expected success InsertSampleData but error, %v", err)
			}
			t.Logf("inserted %d rows", c)
			samples, count, err := pg.GetSamples()
			if err != nil {
				t.Errorf("expected success GetSamples but error, %v", err)
			}
			if count <= 0 {
				t.Errorf("expected count >0, but got %d", count)
			}
			t.Logf("samples: %+v", samples)
		})
	}

}
